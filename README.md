# sfn2014-poster

This is my poster for the 2014 Society for Neuroscience conference.

The main file is `poster.svg`, an Inkscape SVG.

There are a couple [Google fonts](https://code.google.com/p/googlefontdirectory/)
being used (Muli and Tenor Sans). They can be installed from the AUR on Arch
as a set or you could install them individually.

I've also made a little Bash script to iterate through the commit history
and make a video of the poster's progress.
